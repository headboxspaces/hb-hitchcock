# Frontend Code Challenge

## Run the project

```bash
yarn dev
```

## Requirements

You are tasked with crafting a simple movie trailer app which can play a series of short videos.

- you must create a custom video player;
- a user has the following player controls: Play/Pause, Stop;
- the player should display progress but there is no need to make this interactive;
- player volume can be controlled by native functionality/hardware;
- a user can select and play clips at will — selecting a new clip will stop the current clip and start the new one; and,
- the experience must (at a minimum) support latest stable releases of Google Chrome and Safari and needs to display well on smartphone and laptop screen adaptions.

Please use [this XML file](https://ia800306.us.archive.org/13/items/HitchcockTrailers/HitchcockTrailers_files.xml) as your data source. It will provide you with film names and video files, as well as lots of other useful information.

The app should make some kind of request and then parse the data.

Things we care about:

- a beautifully presented interface;
- a slick and seamless user experience; and,
- something that works for different people with different needs.

Notes:

- We encourage you to use GIT to create the project but please don't publish your submission, just send it to us in a .zip file.
- Remember to document how to setup and run the project locally and detail any key technical/design decisions.
- If you have any questions about the task please don't hesitate to reach out, we enjoy and encourage open communication.
