import { useRef, useState, useEffect, useContext } from 'react'
import { NextPage } from 'next'
import {
  Grommet,
  Grid,
  Header,
  Heading,
  Main,
  Footer,
  Box,
  Stack,
  Text,
  Video,
  ResponsiveContext
} from 'grommet';
import { Multimedia as MultimediaIcon } from 'grommet-icons'
import { grommet } from 'grommet/themes'
import { FillType, DirectionType } from 'grommet/utils'
import dynamic from 'next/dynamic'
import { parse } from 'fast-xml-parser'

const ReactFreezeframe: React.ComponentType<{
  src: string
}> = dynamic(() => import('react-freezeframe'), { ssr: false })

enum FileFormats {
  'h.264' = 'h264' as any,
  'MPEG2' = 'mpg' as any,
  'Ogg Video' = 'ogg' as any,
  '512Kb MPEG4' = 'mp4' as any,
}
interface VideoFile {
  src: string
  format: string
}
interface Trailer {
  title: string
  thumbnail?: string
  videoFiles?: VideoFile[]
}

const ENDPOINT: string = 'https://ia800306.us.archive.org/13/items/HitchcockTrailers'

const Viewer: React.FC<{
  trailers: Trailer[]
}> = ({ trailers }): JSX.Element => {
  const [previousTrailer, setPreviousTrailer] = useState<Trailer>(trailers[0])
  const [currentTrailer, setCurrentTrailer] = useState<Trailer>(trailers[0])
  const size = useContext(ResponsiveContext);
  const videoRef = useRef<HTMLVideoElement>(null)
  const grid = size === 'small' ? {
    areas: [
      { name: 'header', start: [0, 0], end: [1, 0] },
      { name: 'main', start: [0, 1], end: [1, 1] },
      { name: 'sidebar', start: [0, 2], end: [1, 2] },
      { name: 'footer', start: [0, 3], end: [1, 3] },
    ],
    columns: ['auto'],
    rows: ['auto', 'auto', 'auto', 'auto']
  } : {
    areas: [
      { name: 'header', start: [0, 0], end: [1, 0] },
      { name: 'main', start: [0, 1], end: [0, 1] },
      { name: 'sidebar', start: [1, 1], end: [1, 1] },
      { name: 'footer', start: [0, 2], end: [1, 2] },
    ],
    columns: ['3/4', '1/4'],
    fill: 'vertical' as FillType,
    rows: ['auto', 'flex', 'auto']
  }

  const sidebar = size === 'small' ? {
    direction: 'row' as DirectionType,
    width: 'small',
    fill: 'horizontal' as FillType,
    pad: { horizontal: 'none', vertical: 'large' }
  } : {
    fill: true,
    pad: 'none',
    width: 'small'
  }

  useEffect(() => {
    if (previousTrailer !== currentTrailer) {
      videoRef.current?.load()
      videoRef.current?.play()
    }
  }, [previousTrailer, currentTrailer])

  return (
    <Grid gap='none' {...grid}>
      <Header gridArea='header' pad='medium' height='xsmall' justify='start'>
        <MultimediaIcon color='accent-1' size='large' />
        <Heading level={1} size='small' color='accent-4'>HeadBox Media Player</Heading>
      </Header>
      <Main gridArea='main' pad={{ horizontal: 'small', vertical: 'none' }}>
        <Video ref={videoRef}>
          {currentTrailer.videoFiles?.map(({ format, src }) => (
            <source key={format} src={src} type={`video/${format}`} />
          ))}
        </Video>
      </Main>
      <Box gridArea='sidebar' overflow='auto' {...sidebar}>
        {
          trailers.map((trailer) => (
            <Box
              background='dark-1'
              flex='grow'
              pad='xsmall'
              height='fit-content'
              margin='xsmall'
              onClick={(): void => {
                setPreviousTrailer(currentTrailer)
                setCurrentTrailer(trailer)
              }}
              key={trailer.title}
              {...trailer === currentTrailer && {
                border: {
                  color: 'accent-1',
                  size: 'small',
                }
              }}
            >
              <ReactFreezeframe src={trailer.thumbnail as string} />
              <Text margin={{ vertical: 'xsmall' }} textAlign='center' truncate={true} size='small'>{trailer.title}</Text>
            </Box>
          ))
        }
      </Box>
      <Footer gridArea='footer' pad='medium' height='xxsmall' />
    </Grid>
  )
}

const Page: NextPage<{
  trailers: Trailer[]
}> = ({ trailers }): JSX.Element => (
  <Grommet theme={grommet} themeMode='dark' full={true}>
    <Viewer {...{ trailers }} />
  </Grommet>
)

Page.getInitialProps = async () => {
  const trailersMap: Map<string, Trailer> = new Map()
  const rawData = await fetch(`${ENDPOINT}/HitchcockTrailers_files.xml`)
  const xmlData = await rawData.text();
  const jsonData = parse(xmlData, {
    attributeNamePrefix: '',
    ignoreAttributes: false,
  }).files.file

  jsonData.forEach((entry: any) => {
    const key = entry.source === 'original' ? entry.name : entry.original

    if (Object.values(FileFormats).includes(entry.format)) {
      const value: Trailer = {
        title: trailersMap.get(key)?.title || entry.title,
        thumbnail: trailersMap.get(key)?.thumbnail,
        videoFiles: [
          ...(trailersMap.get(key)?.videoFiles ? trailersMap.get(key)?.videoFiles : []),
          ...[{
            src: `${ENDPOINT}/${entry.name}`,
            format: FileFormats[entry.format]
          }]
        ]
      }
      trailersMap.set(key, value)
    }

    if (entry.format === 'Animated GIF') {
      const value: Trailer = {
        title: trailersMap.get(key)?.title || entry.title,
        thumbnail: `${ENDPOINT}/${entry.name}`,
        videoFiles: [...(trailersMap.get(key)?.videoFiles ? trailersMap.get(key)?.videoFiles : [])]
      }
      trailersMap.set(key, value)
    }

  });


  return { trailers: Array.from(trailersMap).map(([_, value]) => (value)) }
}

export default Page